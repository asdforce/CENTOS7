#!/bin/bash

#tao csdl va user tuong ung

if [ $(id -u) != "0" ]; then
    printf "Ban phai dang nhap bang user root!\n"
    exit
fi

printf "1. Nhap Database root account: "
read rootUser
printf "\n"

printf "2. Nhap Database root password: "
read rootPass
printf "\n"

printf "3. Create: ten database: "
read subData
printf "\n"

printf "3.1. Create: tai khoan database: "
read subDataName
printf "\n"

printf "3.2. Create: mat khau database: "
read subDataPass
printf "\n"

d_name="c_$subData"
d_user="c_$subDataName"

Q1="CREATE DATABASE IF NOT EXISTS $d_name;"
Q2="CREATE USER '$d_user'@'localhost' IDENTIFIED BY '$subDataPass';"
Q3="GRANT ALL ON $d_name.* TO '$d_user'@'localhost';"
Q4="FLUSH PRIVILEGES;"
SQL="${Q1}${Q2}${Q3}${Q4}"

mysql -u $rootUser -p$rootPass -e "$SQL"

printf "=========================================================================\n"
echo "Da tao thanh cong database: $d_name => $d_user/$subDataPass "
